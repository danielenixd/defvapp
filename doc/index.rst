.. defvapp documentation master file, created by
   sphinx-quickstart on Thu Feb 11 17:58:11 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to defvapp's documentation!
===================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   io_ficheiros
   io_terminal
   main
   utilizadores
   veiculos


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
