import pickle


def le_de_ficheiro(nome_ficheiro):
    """ Le os dados de um ficheiro

    :param nome_ficheiro: Nome do ficheiro onde estao os dados
    :return: O que leu do ficheiro (depende dos dados guardados)
    """

    with open(nome_ficheiro, "rb") as f:
        return pickle.load(f)


def guarda_em_ficheiro(nome_do_ficheiro, dados):
    """ Guarda os dados recebidos num ficheiro

    :param nome_do_ficheiro: Nome do ficheiro onde estao os dados
    :param dados: Dados a serem guardados
    """

    with open(nome_do_ficheiro, "wb") as f:
        pickle.dump(dados, f)

def carrega_as_listas_dos_ficheiros(nome_ficheiro_lista_de_veiculos,
                                    nome_ficheiro_lista_de_utilizadores):
    """ Carrega as listas dos ficheiros
    :param nome_ficheiro_lista_de_veiculos: Carrega a lista de veiculos importando-os para o ficheiro main.py
    :param nome_ficheiro_lista_de_utilizadores: Carrega a lista de utilizadores importando-os para o ficheiro main.py
    """

    lista_de_veiculos = le_de_ficheiro(nome_ficheiro_lista_de_veiculos)
    lista_de_utilizadores = le_de_ficheiro(nome_ficheiro_lista_de_utilizadores)
    return  lista_de_veiculos, lista_de_utilizadores


def guarda_as_listas_em_ficheiros(lista_de_veiculos,
                                  lista_de_utilizadores,
                                  nome_ficheiro_lista_de_veiculos,
                                  nome_ficheiro_lista_de_utilizadores
                                  ):
    """ Carrega as listas dos ficheiros
    :param lista_de_veiculos: Guarda o veiculo criado, adicionando para a lista dos veiculos, fazendo a importacao para o main.py
    :param lista_de_utilizadores: Guarda o utilizador criado, adicionando para a lista de utilizadores, fazendo importacao para main.py
    :param nome_ficheiro_lista_de_veiculos: Carrega a lista de veiculos importando-os para o ficheiro main.py
    :param nome_ficheiro_lista_de_utilizadores: Carrega a lista de utilizadores importando-os para o ficheiro main.py
    """

    op = input("Os dados nos ficheiros serão sobrepostos. Continuar (S/n)?")
    if op in ['s', 'S']:
        guarda_em_ficheiro(nome_ficheiro_lista_de_veiculos, lista_de_veiculos)
        guarda_em_ficheiro(nome_ficheiro_lista_de_utilizadores, lista_de_utilizadores)
    else:
        print("Cancelada.")
