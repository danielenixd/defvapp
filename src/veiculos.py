from io_terminal import imprime_lista

nome_ficheiro_lista_de_veiculos = "lista_de_veiculos.pk"


def cria_novo_veiculo():
    """ Pede ao utilizador para introduzir um novo veiculo

    :return: dicionario com um veiculo na forma
        {"tipo do veiculo": <<tipoveiculo>>, "marca": <<marca>>, "modelo": <<modeloautomovel>>, "cor": <<cor>>, "tipo de caixa:": <<caixa>>, "lotacao:": <<lotacao>>, "numero de portas": <<nrportas>>, "combustivel": <<combustivel>>, "matricula": <<matricula>>, "ano do carro": <<anodoveiculo>>, "preco": <<preco>>}
    """

    #Id comprador
    id_veiculo = gera_id_veiculo()

    # Tipo de veiculo
    tipoveiculo = escolher_tipo_veiculo()

    # Marca do veiculo
    marca = input("marca? ")

    # Modelo do veiculo
    modeloautomovel = input("modelo? ")

    # Cor do veiculo
    cor = input("cor? ").upper()

    # Tipo de caixa

    caixa = escolher_tipo_caixa()

    # Lotacao do veiculo
    lotacao = int(input("lotacao? "))

    # Numero de portas do veiculo
    nrportas = int(input("numero de portas? "))

    # Tipo de combustivel
    combustivel = escolher_tipo_combustivel()

    # Matricula do veiculo
    matricula = input("matricula? ").upper()

    # Ano do veiculo
    anodoveiculo = input("ano do veiculo? ")

    # Preco do veiculo
    preco = input("preco do veiculo? ")

    return {"IdVeiculo": id_veiculo, "tipo do veiculo": tipoveiculo, "marca": marca, "modelo": modeloautomovel, "cor": cor, "tipo de caixa:": caixa, "lotacao:": lotacao, "numero de portas": nrportas, "combustivel": combustivel, "matricula": matricula, "ano do carro": anodoveiculo, "preco": preco}
    print("\n")


def imprime_lista_de_veiculos(lista_de_veiculos):
    """ imprime o cabecalo e a lista dos veiculos
    :param lista_de_veiculos: mostra a lista do veiculo adicionado importando para o main.py
    """


    imprime_lista(cabecalho="Lista de Veiculos", lista=lista_de_veiculos)


#### Funcoes criadas... ####

def escolher_tipo_veiculo():
    """escolher o tipo de veiculo"""
    while True:
        tipo = int(input("Qual e o tipo de veiculo? \n" +
                         "1. Carro \n" +
                         "2. Mota \n " +
                         "Por favor, digite: \n"))

        if tipo == 1:
            tipotransporte = 'Carro'
            break
        elif tipo == 2:
            tipotransporte = 'Mota'
            break
        else:
            print("Entrada invalida..")



def escolher_tipo_caixa():
    """escolher o tipo de caixa"""
    while True:
        tipo = int(input("Tipo de caixa? \n" +
                         "1. Manual \n" +
                         "2. Automatico \n " +
                         "Por favor, digite: \n"))

        if tipo == 1:
            tipodecaixa = 'Manual'
            break
        elif tipo == 2:
            tipodecaixa = 'Automatico'
            break
        else:
            print("Entrada invalida..")


def escolher_tipo_combustivel():
    """escolher o tipo de combustivel"""
    while True:
        tipo = int(input("Qual e o tipo de combustivel? \n" +
                         "1. Gasolina \n" +
                         "2. Diesel \n " +
                         "Por favor, digite: \n"))

        if tipo == 1:
            tipodecombustivel = 'Gasolina'
            break
        elif tipo == 2:
            tipodecombustivel = 'Diesel'
            break
        else:
            print("Entrada invalida..")




def gera_id_veiculo():
    """ Gera autimaticamente o id do veiculo

    :return: return random.randint(100000, 999999)
    """
    import random
    return random.randint(100000, 999999)

