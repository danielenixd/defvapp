from pprint import pprint


def imprime_lista(cabecalho, lista):
    """ Imprime a lista

    :param cabecalho: Cabecalho para a listagem do ficheiro utilizador ou veiculo
    :param lista: Lista a ser impressa dos ficheiros utilizador e veiculo
    """

    cabecalho = f":::::::::::::::::: {cabecalho} ::::::::::::::::::"
    comprimento_cabecalho = len(cabecalho)

    print(cabecalho)
    for id, item in enumerate(lista):
        pprint(f"[{id}]: {item}")
    print(comprimento_cabecalho * ":")


def pergunta_id(questao, lista):
    """Pergunta o ID

    :param questao: Variavel para perguntar qual o id do veiculo ou do utilizador importando-os para main.py
    :param lista: Variavel para para encontrar o veiculo ou utilizador existente
    :return id: Retorna para o ponto do ID existente caso nao existir esse ID
    """

    while True:
        id = int(input(questao))
        if 0 <= id < len(lista):
            return id
        else:
            print(f"id inexistente. Tente de novo. Valores admitidos {0} - {len(lista)}")
