from io_terminal import imprime_lista

nome_ficheiro_lista_de_utilizadores = "lista_de_utilizadores.pk"

def cria_novo_utilizador():
    """ Pede os dados para criar um novo utilizador

    :return: Dicionario com o novo utilizador, {"IdUtilizador": id_utilizador, "nome": nome, "email": email, "morada": morada, "numero de telemovel":numtelemovel, "codigo postal": codpostal}
    """

    #Id comprador
    id_utilizador = gerar_id_comprador()

    # Nome do utilizador
    nome = input("nome? ")

    # Nome do email
    email = input("email? ")

    # Nome da morada
    morada = input("morada? ")

    # Numero de telemovel
    numtelemovel = int(input("numero de telemovel? "))

    # Numero do codigo postal
    codpostal = input("numero do codigo postal? ")

    return {"IdUtilizador": id_utilizador, "nome": nome, "email": email, "morada": morada, "numero de telemovel":numtelemovel, "codigo postal": codpostal}
    print("\n")

def gerar_id_comprador():
    """ Gera o ID para o utilizador

    :return: gera numero aleatorio de 100000 a 999999
    """
    import random
    return random.randint(100000, 999999)




def imprime_lista_de_utilizadores(lista_de_utilizadores):
    """ ..."""

    imprime_lista(cabecalho="Lista de Utilizadores", lista=lista_de_utilizadores)
